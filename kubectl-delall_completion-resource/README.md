# Kubectl delall

* Delete all occurences of specified resource which name contains pattern
* A list of deleted resources will be displayed before confirmation

## Type of completion: resource

## Usage

__kubectl delall [resource-type] [resource-name]__

```bash
kubectl delall service-account toto
```

