# Kubectl bash

* Command to list (and filter) pods by node selector

## Type of completion: none

## Usage

__kubectl list-pods-on-nodepool <namespace> [node-name] <pod-regex>__

```bash
# Use current namespace command
kubectl list-pods-on-nodepool node-foo

# Specify context
kubectl -n namespace list-pods-on-nodepool  node-foo

# Filter by pod name contains
kubectl list-pods-on-nodepool  node-foo pod-bar
```

