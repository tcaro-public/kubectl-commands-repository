* kubectl bash [pod]: connect into pod with bash (pod completion)
```bash
# Connect to the pod with bash (pod completion)
kubectl bash api-foo-4
```
* kubectl sh [pod]: connect into pod with shell (pod completion)
```
kubectl bash api-foo-4
```
* kubectl current-namespace: display current namespace (no completion)
```
kubectl current-namespace
```
* kubectl jget [resource] [name]: get resource information in json output. Extra : open fx if installed (resource completion) https://github.com/antonmedv/fx
```
kubectl jget pod api-foo-4
```
* kubectl pods-not-running: get all pods not in Running status (no completion)
```
kubectl pods-not-running
```
* kubectl delall [resource] [name-pattern] : delete all resources which names contains name-pattern
```
kubectl delall namespaces ns-foo-
```
* kubectl watch [resource] : Run watch kubectl in plain mode
```
kubectl watch pods
```
* kubectl pods-eviction [name-pattern] : Evict properly each pod which match name-pattern (using deployment template annotation)
```
kubectl pods-eviction api-foo
```