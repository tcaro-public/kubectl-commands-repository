# Kubectl stackdriver

* Will open a browser page on Google stackdriver with a query matching the given pod/deployment

## Type of completion: pod

## Usage

__kubectl stackdriver

```bash
# Open for a pod using the current context
kubectl stackdriver api-foo-sfgsdfg12-cfq1

# Specify the context (e.g. if using through k9s)
kubectl stackdriver api-foo -c production -n my-namespace -t resource.labels.container_name
```