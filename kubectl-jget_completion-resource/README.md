# Kubectl jget

* Get resource in json format
* If you have [fx](https://github.com/antonmedv/fx) installed, info will be displayed in FX.<br/>
If you specified -it option, fx will be in interactive mode

## Type of completion: resource

## Usage

__kubectl jget [resource-type] [resource-name]__

```bash
# Simple display (no collapse/expand/query in fx)
kubectl jget pod pod-foo

# Interactive mode
kubectl jget node node-foo -it
```

