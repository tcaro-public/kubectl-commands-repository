# Kubectl sh

* Command to connect into the pod with shell

## Type of completion: pod

## Usage

__kubectl bash [pod-name]__

```bash
# Use current namespace command
kubectl sh pod-foo-n

# Specify context
kubectl sh -n my-namespace pod-foo-n
```

