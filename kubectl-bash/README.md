# Kubectl bash

* Command to connect into the pod with /bin/bash

## Type of completion: pod

## Usage

__kubectl bash [pod-name]__

```bash
# Use current namespace command
kubectl bash pod-foo-n

# Specify context
kubectl bash -n my-namespace pod-foo-n
```

