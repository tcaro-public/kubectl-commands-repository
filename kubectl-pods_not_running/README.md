# Kubectl pods-not-running

* Will display pod names are not in __Running__ state (pending, terminating...)

## Type of completion: none

## Usage

__kubectl pod-not-running

```bash
# Use current namespace command
kubectl pod-not-running

# Specify context
kubectl pod-not-running -n my-namespace
```

