# Kubectl pods-eviction
* Will change all pods which match input pattern
* Use deployment to do it (change pod template annotation)

## Type of completion: none

## Usage

__kubectl pods-eviction [search-pattern]__
```bash
# Use current namespace command
kubectl pods-eviction api-foo

# Specify namespace
kubectl pod-eviction -n my-namespace api-foo

# Disable user validation
kubectl pods-eviction api-foo --force
```

